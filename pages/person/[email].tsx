import { useRouter } from 'next/router';
import React, { useEffect, useRef, useState } from 'react';
import { Button, PageHeader, Descriptions, Input, message, Form } from 'antd';

import { withContextInitialized } from '../../components/hoc';
import CompanyCard from '../../components/molecules/CompanyCard';
import GenericList from '../../components/organisms/GenericList';
import OverlaySpinner from '../../components/molecules/OverlaySpinner';
import { usePersonInformation } from '../../components/hooks/usePersonInformation';

import { Company } from '../../constants/types';
import { ResponsiveListCard } from '../../constants';
import { STORAGE_NAME } from '../../helpers/constants';

const PersonDetail = () => {
  const router = useRouter();
  const [userInfo, setUserInfo] = useState({});
  const { load, loading, save, data } = usePersonInformation(
    router.query?.email as string,
    true
  );

  useEffect(() => {
    load();
  }, []);

  if (loading) {
    return <OverlaySpinner title={`Loading ${router.query?.email} information`} />;
  }

  if (!data) {
    message.error("The user doesn't exist redirecting back...", 2, () =>
      router.push('/home')
    );
    return <></>;
  }

  const handleEditUser = () => {
    save(userInfo);
  };

  const submirForm = (userInfo) => {
    const newUserInfo = {
      ...data,
      name: userInfo.Name,
      birthday: userInfo.Birthday,
      phone: userInfo.Phone,
      gender: userInfo.Gender,
    };
    setUserInfo(newUserInfo);

    const users = localStorage.getItem(STORAGE_NAME);
    const parsedUser = JSON.parse(users);
    if (parsedUser.length) {
      const selectedUser = parsedUser.find((user) => user.id === data.id);
      const index = parsedUser.findIndex((user) => user.id === data.id);
      if (index === -1) {
        parsedUser.push(newUserInfo);
        localStorage.setItem(STORAGE_NAME, JSON.stringify(parsedUser));
      } else {
        const newUser = { ...selectedUser, ...newUserInfo };
        parsedUser[index] = newUser;
        localStorage.setItem(STORAGE_NAME, JSON.stringify(parsedUser));
      }
    } else {
      localStorage.setItem(STORAGE_NAME, JSON.stringify([newUserInfo]));
    }
  };

  return (
    <>
      <PageHeader
        onBack={router.back}
        title="Person"
        subTitle="Profile"
        extra={[
          <Button
            style={{ padding: 0, margin: 0 }}
            type="link"
            href={data.website}
            target="_blank"
            rel="noopener noreferrer"
          >
            Visit website
          </Button>,
          <Button type="default" onClick={handleEditUser}>
            Edit
          </Button>,
        ]}
      >
        {data && (
          <Descriptions size="small" column={1}>
            <Descriptions.Item label="Name">{data.name}</Descriptions.Item>
            <Descriptions.Item label="Gender">{data.gender}</Descriptions.Item>
            <Descriptions.Item label="Phone">{data.phone}</Descriptions.Item>
            <Descriptions.Item label="Birthday">{data.birthday}</Descriptions.Item>
          </Descriptions>
        )}

        <div>
          Edit
          <Form
            name="basic"
            labelCol={{ span: 8 }}
            wrapperCol={{ span: 16 }}
            initialValues={{
              Name: data.name,
              Gender: data.gender,
              Phone: data.phone,
              Birthday: data.birthday,
            }}
            onFinish={(data) => submirForm(data)}
            //onFinishFailed={onFinishFailed}
            autoComplete="off"
          >
            <Form.Item label="Name" name="Name">
              <Input />
            </Form.Item>

            <Form.Item label="Gender" name="Gender">
              <Input />
            </Form.Item>

            <Form.Item label="Phone" name="Phone">
              <Input />
            </Form.Item>

            <Form.Item label="Birthday" name="Birthday">
              <Input />
            </Form.Item>

            <Form.Item wrapperCol={{ offset: 8, span: 16 }}>
              <Button type="primary" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
        <GenericList<Company>
          loading={loading}
          extra={ResponsiveListCard}
          data={data && data.companyHistory}
          ItemRenderer={({ item }: any) => <CompanyCard item={item} />}
          handleLoadMore={() => {}}
          hasMore={false}
        />
      </PageHeader>
    </>
  );
};

export default withContextInitialized(PersonDetail);
